import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

// General Pages
import NotFound from '@/views/NotFound.vue'
import Login from '@/views/Login.vue'
import Register from '@/views/Register.vue'

// Dashbaord Pages
import Home from '@/views/dashboard/Home.vue'
import Users from '@/views/dashboard/Users.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
      {
        path: '/login',
        name: 'Login',
        component: Login
      },
      {
        path: '/register',
        name: 'Register',
        component: Register
      },
    {
      path: '/',
      redirect: '/dashboard',
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/dashboard',
      name: 'Home',
      component: Home,
      meta: {
        requiredAuth: true
      },

    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      meta: {
        requiredAuth: true
      },
  },
    { path: '*', component: NotFound }
  ]
})

router.beforeEach(function (to, from, next) {
    if (to.meta.requiredAuth) {
        if (store.getters.getProfile.id) {
            next()
        } else {
            router.push('/login')
        }
    } else {
        next()
    }
})

export default router
