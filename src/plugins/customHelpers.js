const customHelpers = {
    install (Vue, options) {
        Vue.prototype.$helpers = {

            getCurrentDatetime () {
                var currentdate = new Date()
                return currentdate.getFullYear() + '-' + this.twoDigits(1 + currentdate.getUTCMonth()) + '-' + this.twoDigits(currentdate.getUTCDate()) + ' ' + currentdate.getHours() + ':' + currentdate.getMinutes() + ':' + currentdate.getSeconds()
            },

            twoDigits (d) {
                if (d >= 0 && d < 10) return '0' + d.toString()
                if (d > -10 && d < 0) return '-0' + (-1 * d).toString()
                return d.toString()
            },

            capitalizeFirstLetter (string) {
                return string.charAt(0).toUpperCase() + string.slice(1)
            },

        }
    }
}

export default customHelpers
