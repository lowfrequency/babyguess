import firebase from 'firebase/app';
import 'firebase/auth'
import 'firebase/firestore'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyAsAr8Lu1DO0BonZD-MVdLfHrSBM0mO-0w",
    authDomain: "baby-guess-48eab.firebaseapp.com",
    databaseURL: "https://baby-guess-48eab.firebaseio.com",
    projectId: "baby-guess-48eab",
    storageBucket: "baby-guess-48eab.appspot.com",
    messagingSenderId: "1007157565013",
    appId: "1:1007157565013:web:71ecce8b4dde482f"

})

export const firebaseDb = firebaseApp.firestore()
export const firebaseAuth = firebaseApp.auth()
