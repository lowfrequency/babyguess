import Vue from 'vue'
import '@/plugins/vuetify'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import '@/registerServiceWorker'

import VueFirestore from 'vue-firestore';
Vue.use(VueFirestore);

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import customHelpers from '@/plugins/customHelpers.js'
Vue.use(customHelpers)

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
