import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import menu from './modules/menu'
import globals from './modules/globals'
import guesses from './modules/guesses'
import notifications from './modules/notifications'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth: auth,
        menu: menu,
        globals: globals,
        guesses: guesses,
        notifications: notifications
    }
})
