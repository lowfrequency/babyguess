export default {
  api: 'https://frameworkapi.wpengine.com/wp-json',
  login: '/jwt-auth/v1/token',
  profile: '/wp/v2/users/me',
  users: '/wp/v2/users/',
  menu: '/menus/v1/menus/api-menu',
  posts: '/wp/v2/posts'
}
