/* Loading State */

const defaultState = {
    show: false,
    success: '',
    error: ''
}

const getters = {
    showNotification: state => state.show,
    successNotification: state => state.success,
    errorNotification: state => state.error
}

const actions = {
    remove_notifications ({ commit }) {
        commit('NOTIFICATIONS_RESET')
    },
    success_notifications ({ commit }, message) {
        commit('NOTIFICATIONS_SUCCESS', message)
        setTimeout(
            function () {
                commit('NOTIFICATIONS_RESET')
            },
        4000)
    },
    error_notifications ({ commit }, message) {
        commit('NOTIFICATIONS_ERROR', message)
        setTimeout(
            function () {
                commit('NOTIFICATIONS_RESET')
            },
        4000)
    }
}

const mutations = {
    NOTIFICATIONS_SUCCESS: (state, message) => {
        state.show = true
        state.success = message
        state.error = ''
    },
    NOTIFICATIONS_ERROR: (state, message) => {
        state.show = true
        state.success = ''
        state.error = message
    },
    NOTIFICATIONS_RESET: (state) => {
        state.show = false
        state.success = ''
        state.error = ''
    }
}

export default {
    state: defaultState,
    getters,
    actions,
    mutations
}
