/* Posts State */
import { firebaseDb } from '@/firebase'

const guessesCollection = firebaseDb.collection('Guess')

const defaultState = {
  guesses: JSON.parse(localStorage.getItem('guesses')) || [],
  headers: [
        //{ text: 'ID', value: 'id', align: 'left' },
        { text: 'Name', value: 'name', align: 'left' },
        { text: 'Sex', value: 'sex', align: 'left' },
        { text: 'Weight', value: 'weight', align: 'left' },
        { text: 'Date', value: 'date', align: 'left' },
    ]
}

const getters = {
    getGuesses: state => state.guesses,
    getGuessHeaders: state => state.headers
}

const actions = {

    get_guesses({ commit }){
        return new Promise((resolve, reject) => {
            guessesCollection.get()
            .then(function(querySnapshot) {
                commit('REMOVE_ACCOUNTS')
                querySnapshot.forEach(function(doc) {
                    const guess = doc.data()
                    guess.id = doc.id
                    commit('ADD_ACCOUNT', guess)
                })
                resolve()
            }).catch(function(error) {
                console.log("Error getting documents: ", error)
                reject()
            })
        })
    },

    update_guess ({ commit }, guess, cachedGuess) {
        return new Promise((resolve, reject) => {
            guessesCollection.doc(guess.id).set({
                sex: guess.sex,
                weight: guess.weight,
                date: guess.date,
                user_id: guess.user_id,
            })
            .then(function() {
                commit('EDIT_ACCOUNT', guess)
                resolve()
            })
            .catch(function(error) {
                console.log(error)
                commit('EDIT_ACCOUNT', cachedGuess)
                reject()
            });
        })
    },

    create_guess ({ commit }, guess) {
        return new Promise((resolve, reject) => {
            guessesCollection.add(guess)
            .then(function (docRef) {
                commit('ADD_ACCOUNT', guess)
                resolve()
            })
            .catch(function (error) {
                console.log( error )
                reject()
            })
        })
    },

    clear_guesses ({ commit }) {
        return new Promise((resolve, reject) => {
            commit('REMOVE_ACCOUNTS')
            resolve()
        })
    }
}

const mutations = {
    ADD_ACCOUNT: (state, guess) => {
        state.guesses.push(guess)
        localStorage.setItem('guesses', JSON.stringify(state.guesses))
    },
    REMOVE_ACCOUNTS: (state,) => {
        state.guesses = []
        localStorage.setItem('guesses', JSON.stringify(state.guesses))
    },
    EDIT_ACCOUNT: (state, guess) => {
        var item = state.guesses.find(loopGuess => loopGuess.id === guess.id)
        item.sex = guess.sex
        item.weight = guess.weight
        item.date = guess.date
    }
}

export default {
    state: defaultState,
    getters,
    actions,
    mutations
}
