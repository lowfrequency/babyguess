/* Authentication State */
import { firebaseDb } from '@/firebase'

const usersCollection = firebaseDb.collection('Users')

const state = {
    profile: JSON.parse(localStorage.getItem('profile')) || {
        id: '',
        name: '',
    },
    users: JSON.parse(localStorage.getItem('users')) || [],
    headers: [
          { text: 'ID', value: 'id', align: 'left' },
          { text: 'Name', value: 'name', align: 'left' },
          { text: 'Approved', value: 'approved', align: 'left' },
          { text: 'Actions', value: 'actions', sortable: false }
      ]
}

const getters = {
    getID: state => state.profile.id,
    getProfile: state => state.profile,
    getName: state => state.profile.name,
    isAuthenticated: state => state.profile.id,
    getUsers: state => state.users,
    getUsersHeaders: state => state.headers
}

const actions = {

    logout ({ commit, dispatch }) {
        commit('AUTH_LOGOUT')
        commit('REMOVE_USERS')
    },

    login ({ commit, dispatch }, credentials) {
        return new Promise((resolve, reject) => {
            usersCollection
            .where('name', '==', credentials.name)
            .where('password', '==', credentials.password)
            //.where('approved', '==', true)
            .limit(1)
            .get()
                .then(function(querySnapshot) {
                    //commit('REMOVE_ACCOUNTS')
                    if(querySnapshot.empty){
                        var error = 'name or password was incorrect'
                        reject(error)
                    } else {
                        const queryDocumentSnapshot = querySnapshot.docs[0].data()
                        var user = {
                            id: querySnapshot.docs[0].id,
                            name: queryDocumentSnapshot.name,
                            approved: queryDocumentSnapshot.approved,
                            admin: queryDocumentSnapshot.admin
                        }
                        commit('USER_SUCCESS', user)
                        resolve()
                    }
                }).catch(function(error) {
                    console.log("Error getting documents: ", error)
                    var error = 'name or password was incorrect'
                    reject(error)
                })
        })
    },

    register ({ commit, dispatch }, credentials) {
        return new Promise((resolve, reject) => {
            usersCollection.where('name', '==', credentials.name).limit(1).get()
                .then(function(querySnapshot) {
                    //commit('REMOVE_ACCOUNTS')
                    if(!querySnapshot.empty){
                        var error = 'name already has an account'
                        reject(error)
                    } else {
                        var user = {
                            name: credentials.name,
                            password: credentials.password,
                            approved: false,
                            admin: false
                        }
                        usersCollection.add(user)
                        .then(function (docRef) {
                            user.id = docRef.id
                            commit('USER_SUCCESS', user)
                            resolve()
                        })
                        .catch(function (error) {
                            console.log( error )
                            var error = 'Oops something went wrong please try again'
                            reject(error)
                        })
                    }
                }).catch(function(error) {
                    console.log("Error getting documents: ", error)
                    var error = 'Oops something went wrong please try again'
                    reject(error)
                })
        })
    },

    get_users({ commit }){
        return new Promise((resolve, reject) => {
            usersCollection.get()
            .then(function(querySnapshot) {
                commit('REMOVE_USERS')
                querySnapshot.forEach(function(doc) {
                    const user = doc.data();
                    user.id = doc.id
                    commit('ADD_USERS', user)
                })
                resolve()
            }).catch(function(error) {
                console.log("Error getting documents: ", error)
                reject()
            })
        })
    },

    update_user ({ commit }, user, approved) {
        return new Promise((resolve, reject) => {
            usersCollection.doc(user.id).update({
                approved: user.approved
            })
            .then(function() {
                resolve()
            })
            .catch(function(error) {
                console.log(error)
                reject()
            });
        })
    },

    delete_user ({ commit }, user) {
        return new Promise((resolve, reject) => {
            usersCollection.doc(user.id).delete()
            .then(function (docRef) {
                resolve()
            })
            .catch(function (error) {
                console.log( error )
                reject()
            })
        })
    },

}

const mutations = {
    ADD_USERS: (state, user) => {
        state.users.push(user)
        localStorage.setItem('users', JSON.stringify(state.users))
    },
    REMOVE_USERS: (state) => {
        state.users = []
        localStorage.removeItem('users')
    },
    USER_SUCCESS: (state, user) => {
        state.profile = user
        localStorage.setItem('profile', JSON.stringify(state.profile))
    },
    AUTH_LOGOUT: (state) => {
        state.profile = {}
        localStorage.removeItem('profile')
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}
