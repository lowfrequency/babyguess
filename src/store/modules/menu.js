/* Authentication State */
import Axios from 'axios'
import APIConfig from '@/store/apiConfig'
import store from '@/store'

const state = {
    menu: [
        {
            id: 1,
            title: 'Home',
            icon: 'home',
            link: '/',
            active: true,
        },
        {
            id: 2,
            title: 'Posts',
            icon: 'collections',
            link: '/dashboard/posts',
            active: false,
        },
        {
            id: 3,
            title: 'Accounts',
            icon: 'collections',
            link: '/dashboard/accounts',
            active: false,
        },
        {
            id: 4,
            title: 'Profile',
            icon: 'person',
            link: '/dashboard/profile',
            active: false,
        },
        {
            id: 5,
            title: 'Menu Item',
            icon: 'list',
            link: '/',
            active: false,
        }
    ],
    subscriptions: [
        { picture: 28, text: 'Joseph' },
        { picture: 38, text: 'Apple' },
        { picture: 48, text: 'Xbox Ahoy' },
        { picture: 58, text: 'Nokia' },
        { picture: 78, text: 'MKBHD' }
    ],
    apiMenu: []
}

const getters = {
    getMenu: state => state.menu,
    getSubscriptions: state => state.subscriptions,
    getAPIMenu: state => state.apiMenu.items
}

const actions = {
    get_menu ({ commit, dispatch }) {
        return new Promise((resolve, reject) => {
            Axios.get(APIConfig.api + APIConfig.menu, store.getters.getAuthenticationHeader).then((response) => {
                console.log(response.data)
                commit('API_MENU_SUCCESS', response.data)
                resolve()
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
        })
    },
    set_active_menu ({ commit, dispatch }, menuId) {
        commit('MENU_ACTIVE', menuId)
    }
}

const mutations = {
    API_MENU_SUCCESS: (state, menu) => {
        state.apiMenu = menu
    },
    MENU_ACTIVE: (state, menuId) => {
        state.menu.forEach(function (menu) {
            if (menu.id === menuId) {
                menu.active = true
            } else {
                menu.active = false
            }
        })
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
