/* Sidebar State */

const state = {
    sidebarState: true,
    darkmode: JSON.parse(localStorage.getItem('darkmode')) || false,
    color: 'blue-grey'
}

const getters = {
    isSidebarOpen: state => state.sidebarState,
    darkmode: state => state.darkmode,
    getColor: state => state.color
}

const actions = {
    switchSidebarState ({ commit }) {
        commit('SIDEBAR_STATE_SWITCH')
    },
    switchDarkmode ({ commit }) {
        commit('SIDEBAR_SWITCH_DARKMODE')
    }
}

const mutations = {
    SIDEBAR_STATE_SWITCH: (state) => {
        state.sidebarState = !state.sidebarState
    },
    SIDEBAR_SWITCH_DARKMODE: (state) => {
        state.darkmode = !state.darkmode
        localStorage.setItem('darkmode', state.darkmode)

    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
