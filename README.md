# Baby Guess

This project is a quick proof of concept using Vue and Firestore to manage our families guess for our second child. 
Is it going to be a girl or boy? How heavy is it going to be?
This app quickly allows users to register and provide their guess and see all guesses.
For admins there is a simple admin functionality to remove unwanted users.

## Setup

### Vue

First clone this project and get your development environment setup. Here is a basic Vue installation steps https://cli.vuejs.org/guide/installation.html

You should install all of the required dependcies
```
npm install
```

I strongly recommend using the Vue UI to run development. Makes things simple https://cli.vuejs.org/guide/creating-a-project.html#using-the-gui

But you can run development with:
```
npm run serve
```

### Firebase

This app uses the firebase as a database. Follow these simple 4 steps to set up an account https://firebase.google.com/docs/firestore/quickstart

Once you have created your project you will be given the database info. Copy and paste this into src\firebase.js

### Register

Now you should be able to register as a new user. You should now have instance access to make a guess and see other guesses.

First you will need to set yourself as admin, go into the firebase collection document and change your 'admin' field to true. Now you should be able to manage other users.

### Deployemnt

Here is a greta article on how to deploy a Vue application https://cli.vuejs.org/guide/deployment.html#general-guidelines

I strongly recommend using Netlify free hosting.

To create for production run and uplaod dist directory
```
npm run build
```

### Security
This app does not use the recommended Firebase auth structures. 
This was a conscious decision because this application will be used a range of family members, including juniors and elderly without email addresses.
It uses a Collection called "Users" and stores the password in plain text in the documents.
Please make sure all users are aware that this is the case and to use a temporary password just for this site.

If you are more security conscious it is a simple change to use the Firebase auth system. Minor updates are required to src/store/modules/auth.js 
